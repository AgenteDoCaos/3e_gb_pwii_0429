import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadimobiliariaComponent } from './cadimobiliaria.component';

const routes: Routes = [
  {
    path: "",
   component: CadimobiliariaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadimobiliariaRoutingModule { }
