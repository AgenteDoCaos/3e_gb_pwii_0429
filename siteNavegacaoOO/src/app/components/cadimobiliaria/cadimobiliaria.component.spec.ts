import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadimobiliariaComponent } from './cadimobiliaria.component';

describe('CadimobiliariaComponent', () => {
  let component: CadimobiliariaComponent;
  let fixture: ComponentFixture<CadimobiliariaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadimobiliariaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadimobiliariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
