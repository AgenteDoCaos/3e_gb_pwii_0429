import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadimobiliariaRoutingModule } from './cadimobiliaria-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadimobiliariaRoutingModule
  ]
})
export class CadimobiliariaModule { }
