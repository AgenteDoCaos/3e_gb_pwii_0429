import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadimovelComponent } from './cadimovel.component';

const routes: Routes = [
  {
    path: "",
   component: CadimovelComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadimovelRoutingModule { }
