import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadimovelRoutingModule } from './cadimovel-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadimovelRoutingModule
  ]
})
export class CadimovelModule { }
