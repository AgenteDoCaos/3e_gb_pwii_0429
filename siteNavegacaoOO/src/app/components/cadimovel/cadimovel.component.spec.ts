import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadimovelComponent } from './cadimovel.component';

describe('CadimovelComponent', () => {
  let component: CadimovelComponent;
  let fixture: ComponentFixture<CadimovelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadimovelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadimovelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
