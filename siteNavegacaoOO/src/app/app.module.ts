import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadimovelComponent } from './components/cadimovel/cadimovel.component';
import { CadimobiliariaComponent } from './components/cadimobiliaria/cadimobiliaria.component';
import { CadProprietarioComponent } from './components/cad-proprietario/cad-proprietario.component';
import { CadLocadorComponent } from './components/cad-locador/cad-locador.component';

@NgModule({
  declarations: [
    AppComponent,
    CadimovelComponent,
    CadimobiliariaComponent,
    CadProprietarioComponent,
    CadLocadorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
